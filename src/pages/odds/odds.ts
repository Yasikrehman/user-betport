import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http';
import { OddsbetPage } from '../oddsbet/oddsbet';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

/**
 * Generated class for the OddsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-odds',
  templateUrl: 'odds.html',
})
export class OddsPage {
  name:String;
  total_balance:String;
  total_limit:String;
  total_exposure:String;
  odds_data:any;
  result:any;
  odds_id:String;
  count:any;
  subscription:any;
  user_id:any;
  bet_result:any;

  team1:any;
  team2:any;
  team3:any;
  team1_color:any;
  team2_color:any;
  team3_color:any;
  constructor(public http: HTTP, public navCtrl: NavController,public storage: Storage, public navParams: NavParams) {
  	
	  this.odds_id = navParams.get('id');
	  this.storage.get('match_name').then((val) => {
	    this.name = val;
	  });
      this.storage.get('total_balance').then((val) => {
	    this.total_balance = val;
	  });
	  this.storage.get('total_exposure').then((val) => {
		    this.total_exposure = val;
	  });
	  this.storage.get('user_id').then((val) => {
	    this.user_id = val;
	  });

	  this.team1 = 0;
	  this.team2 = 0;
	  this.team3 = 0;
	  this.team1_color = "secondary";
	  this.team2_color = "secondary";
	  this.team3_color = "secondary";

  }

  ionViewDidEnter(){
     this.load_bet_details();
  }
  ionViewDidLoad() {
    this.load_content();
    console.log('ionViewDidLoad OddsPage');
    this.subscription = Observable.interval(1000).subscribe(x => {
      this.load_content();
    });
  }
  load_bet_details(){
    let postData = {
          "user_id": '24',
          "eventid": this.odds_id
    }
    this.http.post('http://betport.bookoevent.com/public/api/getBettingDetails', postData, {})
    .then(data => {
        this.bet_result = JSON.parse(data.data);

        this.total_balance=this.bet_result.data.user_details.total_point;
        this.total_limit=this.bet_result.data.user_details.total_point;
        this.total_exposure=this.bet_result.data.user_details.exposure;

        if(this.bet_result.message == "Betted"){
            this.team1 = this.bet_result.data.betting_details.team1_odds_balance;
	  		this.team2 = this.bet_result.data.betting_details.team2_odds_balance;
	        this.team3 = this.bet_result.data.betting_details.team3_odds_balance;

	        if(this.team1 < 0){
	           this.team1_color = "danger";
	        }
	        if(this.team2 < 0){
	           this.team2_color = "danger";
	        }
	        if(this.team3 < 0){
	           this.team3_color = "danger";
	        }
        }else{
            this.team1 = 0;
	  		this.team2 = 0;
	        this.team3 = 0;
			this.team1_color = "secondary";
	  		this.team2_color = "secondary";
	  		this.team3_color = "secondary";
        }
    }).catch(error => {
    	alert(error);
    });
  }
  load_content(){
  	this.http.post('http://35.200.243.144/C2.php?sportid=4&eventid='+this.odds_id, {}, {})
    .then(data => {
      this.result = JSON.parse(data.data);

      for(this.count = 0; this.count < this.result.result.length; this.count++){
         if(this.result.result[this.count].mtype == "MATCH_ODDS"){
         	this.odds_data = this.result.result[this.count];
         	this.odds_data.runners[0].bet = this.team1;
         	this.odds_data.runners[1].bet = this.team2;
         	this.odds_data.runners[0].color = this.team1_color;
         	this.odds_data.runners[1].color = this.team2_color;
            //this.odds_data.runners[0].lay[0].price = 1.99;
           //this.odds_data.runners[1].lay[0].price = 1.99;
         	if(this.odds_data.runners.length > 2){
         	   this.odds_data.runners[2].bet = this.team3;
         		this.odds_data.runners[2].color = this.team3_color;
         	}
         	this.count = this.result.result.length + 1; 
         }
      }
    }).catch(error => {
    	alert(error);
    });
  }

  move_bet_page(name,type,price){
  	this.navCtrl.push(OddsbetPage,{ name:name,type:type,price:price,id:this.odds_id});
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OddsPage } from './odds';

@NgModule({
  declarations: [
    OddsPage,
  ],
  imports: [
    IonicPageModule.forChild(OddsPage),
  ],
})
export class OddsPageModule {}

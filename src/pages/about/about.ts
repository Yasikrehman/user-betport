import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  
  overview_sports: any;
  user_id:String;
  result:any;
  total_points:String;
  exposure:String;
  limit:String;
  overview_cricket:String;
  overview_tennis:String;
  overview_soccer:String;
  constructor(public http: HTTP,public navCtrl: NavController,public storage: Storage) {
    this.user_id = "24";
  }
  ionViewDidLoad() {
    
    this.load_data();
  }
  change_overview_type(val){
    if(val == 1){
    	document.getElementById('cricket_tab').style.display='block';
    	document.getElementById('tennis_tab').style.display='none';
    	document.getElementById('soccer_tab').style.display='none';
    }if(val == 2){
    	document.getElementById('tennis_tab').style.display='block';
    	document.getElementById('cricket_tab').style.display='none';
    	document.getElementById('soccer_tab').style.display='none';
    }if(val == 3){
    	document.getElementById('soccer_tab').style.display='block';
    	document.getElementById('tennis_tab').style.display='none';
    	document.getElementById('cricket_tab').style.display='none';
    }
  	
  }

  load_data(){

     let postData = {
          "id": this.user_id
    }

  	this.http.post('http://betport.bookoevent.com/public/api/profileoverview', postData, {})
    .then(data => {
      this.result = JSON.parse(data.data);
      this.total_points = this.result.data.totalpoints;
      this.exposure = this.result.data.exposure;
      this.limit = this.result.data.user_limit;
      this.overview_cricket = this.result.data.matches.cricket;
      this.overview_tennis = this.result.data.matches.tennis;
      this.overview_soccer = this.result.data.matches.soccer;
    }).catch(error => {
      alert(error);
    });
  }

  segmentChanged()
  {
    
  }
}

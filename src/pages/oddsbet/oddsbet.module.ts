import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OddsbetPage } from './oddsbet';

@NgModule({
  declarations: [
    OddsbetPage,
  ],
  imports: [
    IonicPageModule.forChild(OddsbetPage),
  ],
})
export class OddsbetPageModule {}

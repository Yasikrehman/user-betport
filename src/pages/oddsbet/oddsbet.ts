import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http';
/**
 * Generated class for the OddsbetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-oddsbet',
  templateUrl: 'oddsbet.html',
})
export class OddsbetPage {

  //calculation values	
  rate:any;
  stake:any;
  team1:any;
  team2:any;
  team3:any;
  team1_color:any;
  team2_color:any;
  team3_color:any;
  profit:any;
  profit_color:any;

  name:any;
  type:any;
  price:any;
  total_balance:any;
  total_limit:any;
  total_exposure:any;
  odds_id:String;
  result:any;
  count:any;
  odds_data:any;
  bt_list1:any;
  bt_list2:any;
  bt_list3:any;

  profit_name:any;
  user_id:any;
  event_name:any;
  game_type:any;
  betting_team_id:any;
  bet_result:any;
  current_exposure:any;
  i:any;
  constructor(public http: HTTP,public navCtrl: NavController, public storage: Storage, public navParams: NavParams, public toastCtrl: ToastController) {
	this.name = navParams.get('name');
	this.type = navParams.get('type');
	this.price = navParams.get('price');
	this.odds_id = navParams.get('id');
	storage.get('total_balance').then((val) => {
	    this.total_balance = val;
	});
	storage.get('total_limit').then((val) => {
	    this.total_limit = val;
	});
	storage.get('user_id').then((val) => {
	    this.user_id = val;
	});
	storage.get('total_exposure').then((val) => {
	    this.total_exposure = val;
	});

	this.team1 = 0;
	this.team2 = 0;
	this.team3 = 0;
  }

  ionViewDidLoad() {
  	this.load_content();
    console.log('ionViewDidLoad OddsbetPage');
  }

  load_content(){
  	this.http.post('http://35.200.243.144/C2.php?sportid=4&eventid='+this.odds_id, {}, {})
    .then(data => {
      this.result = JSON.parse(data.data);

      for(this.count = 0; this.count < this.result.result.length; this.count++){
         if(this.result.result[this.count].mtype == "MATCH_ODDS"){
         	this.odds_data = this.result.result[this.count];
         	for(this.i=0;this.i<this.result.result[this.count].runners.length;this.i++){
         		if(this.i == 0){
         			this.bt_list1 = this.result.result[this.count].runners[0].name;
         		}else if(this.i == 1){
         			this.bt_list2 = this.result.result[this.count].runners[1].name;
         		}else if(this.i == 2){
         			this.bt_list3 = this.result.result[this.count].runners[2].name;
         		}
         	}
         	this.event_name = this.result.result[this.count].event.name;
         	this.game_type = this.result.result[this.count].eventTypeId;
         	this.count = this.result.result.length + 1; 
         }
      }
    }).catch(error => {
    	alert(error);
    });
  }

  move_backward(){
  	this.navCtrl.pop();
  }

  append_amount(val){
    	this.stake = parseInt(this.stake) + parseInt(val);
    	this.betting_calculation();
  }

  clear_amount(){
  	this.stake = 0;
  	this.betting_calculation();
  }

  betting_calculation(){

  	
  	this.profit = this.stake * (this.rate-1);
    this.profit = this.profit.toFixed(2);
  	if(this.type == "back"){
  	    this.profit_color = "color-green";
  	    this.profit_name = "Profit";
  		if(this.bt_list1 == this.name){
	    	this.team1 = this.profit;
	    	this.team2 = -this.stake;
	    	this.team3 = -this.stake;

	    	this.team1_color = "color-green";
	    	this.team2_color = "color-red";
	    	this.team3_color = "color-red";
	    }else if(this.bt_list2 == this.name){
	    	this.team1 = -this.stake;
	    	this.team2 = this.profit;
	    	this.team3 = -this.stake;

	    	this.team1_color = "color-red";
	    	this.team2_color = "color-green";
	    	this.team3_color = "color-red";
	    }else  if(this.bt_list3 == this.name){
	    	this.team1 = -this.stake;
	    	this.team2 = -this.stake;
	    	this.team3 = this.profit;

	    	this.team1_color = "color-red";
	    	this.team2_color = "color-red";
	    	this.team3_color = "color-green";
	    }
  	}else if(this.type == "lay"){
  	    this.profit_color = "color-red";
  	    this.profit_name = "Liability";
  		if(this.bt_list1 == this.name){
	    	this.team1 = -this.profit;
	    	this.team2 = this.stake;
	    	this.team3 = this.stake;

	    	this.team1_color = "color-red";
	    	this.team2_color = "color-green";
	    	this.team3_color = "color-green";
	    }else if(this.bt_list2 == this.name){
	    	this.team1 = this.stake;
	    	this.team2 = -this.profit;
	    	this.team3 = this.stake;

	    	this.team1_color = "color-green";
	    	this.team2_color = "color-red";
	    	this.team3_color = "color-green";
	    }else  if(this.bt_list3 == this.name){
	    	this.team1 = this.stake;
	    	this.team2 = this.stake;
	    	this.team3 = -this.profit;

	    	this.team1_color = "color-green";
	    	this.team2_color = "color-green";
	    	this.team3_color = "color-red";
	    }
  	}
    
  }

  place_bet(){
    
    if(this.profit_name == "Profit"){
    	if(parseFloat(this.stake) > parseFloat(this.total_balance)){
	  		const toast = this.toastCtrl.create({
		      message: 'Sorry! Low balance..',
		      duration: 3000
		    });
		    toast.present();
		    return false;
  		}
    }else{
    	if(parseFloat(this.profit) > parseFloat(this.total_balance)){
	  		const toast = this.toastCtrl.create({
		      message: 'Sorry! Low balance..',
		      duration: 3000
		    });
		    toast.present();
		    return false;
  		}
    }
  	

  	let postData = {
          "user_id": this.user_id,
          "eventid": this.odds_id,
          "paid_amount":this.stake,
          "match_type":"ODDS",
          "event_name":this.stake,
          "betting_type":"ODDS",
          "betting_rate":this.stake,
          "betting_on":this.type,
          "betting_to":this.name,
          "match_fee":"100",
          "eligible_amount":this.profit,
          "teamA":this.bt_list1,
          "teamB":this.bt_list2,
          "team1_odds_balance":this.team1,
          "team2_odds_balance":this.team2,
          "team3_odds_balance":this.team3,
          "game_type":this.game_type,
          "betting_team_id":"1234567"
    }

    this.http.post('http://betport.bookoevent.com/public/api/placebet', postData, {})
    .then(data => {
        this.bet_result = JSON.parse(data.data);
        if(this.bet_result){
        	this.storage.set('total_balance', this.bet_result.data.total_point);
        	this.storage.set('total_exposure', this.bet_result.data.exposure);
        	this.total_balance = this.bet_result.data.total_point;
        	this.total_exposure = this.bet_result.data.exposure;

        	const toast = this.toastCtrl.create({
		      message: 'Betting Places Successfully..!',
		      duration: 3000
		    });
		    toast.present();
		    this.navCtrl.pop();
		    //this.navCtrl.push(OddsPage,{id:this.odds_id});
        }else{
        	const toast = this.toastCtrl.create({
		      message: 'Sorry something went wrong..!',
		      duration: 3000
		    });
		    toast.present();
        }
    }).catch(error => {
    	alert(error);
    });
  }

}

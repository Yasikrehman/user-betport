import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController,ToastController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { HTTP } from '@ionic-native/http';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  
  url:string;
  login_details:any;
  @ViewChild('username') uname;
  @ViewChild('password') pass;
  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController,public http: HTTP,public storage: Storage,public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login(){
    if(this.uname.value == "" || this.pass.value == ""){
      
      const toast = this.toastCtrl.create({
        message: 'Username or password missing',
        duration: 3000
      });
      toast.present();
    }else{
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();

      let postData = {
          "username": this.uname.value,
          "password": this.pass.value
      }

      this.http.post('http://betport.bookoevent.com/public/api/userlogin', postData, {})
      .then(data => {
         loading.dismiss();
         this.login_details = JSON.parse(data.data);

         if(this.login_details.success && this.login_details.message != ""){
            this.storage.set('total_balance',this.login_details.data.totalpoints);
            this.storage.set('user_id',this.login_details.data.userid);
            this.storage.set('total_exposure',this.login_details.data.exposure);
            this.storage.set('total_limit',this.login_details.data.user_limit);
            this.navCtrl.push(TabsPage);
        }else{

            const toast = this.toastCtrl.create({
              message: this.login_details.data,
              duration: 3000
            });
            toast.present();
        }

      })
      .catch(error => {

        const toast = this.toastCtrl.create({
          message: error,
          duration: 3000
        });
        toast.present();

      });
    }
  }

}

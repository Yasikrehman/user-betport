import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { Storage } from '@ionic/storage';
import { OddsPage } from '../odds/odds';
import 'rxjs/add/operator/map';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  url:string;
  cricket_data:any;
  tennis_data:any;
  soccer_data:any;
  total_balance:String;
  total_exposure:String;
  total_limit:String;
  result:any;
  pet:String = "cricket";
  constructor(public http: HTTP, public navCtrl: NavController,public storage: Storage) {

  	 storage.get('total_balance').then((val) => {
	    this.total_balance = val;
	 });
	 storage.get('total_exposure').then((val) => {
	    this.total_exposure = val;
	 });
	 storage.get('total_limit').then((val) => {
	    this.total_limit = val;
	 });
  	this.loadData();
  }
  loadData(){
    
    this.http.post('http://betport.bookoevent.com/public/api/upcomingevent', {}, {})
    .then(data => {
       this.result = JSON.parse(data.data);
       this.cricket_data = this.result.data.cricket;
       this.soccer_data = this.result.data.soccer;
       this.tennis_data = this.result.data.tennis;
    }).catch(error => {
      alert(error);
    });

  	/*this.http.post('http://betport.bookoevent.com/public/api/upcomingevent',{},{}})
  	.map(res => res.json())
  	.subscribe(data =>{
  		this.cricket_data = data.data.cricket;
  		this.soccer_data = data.data.soccer;
  		this.tennis_data = data.data.tennis;

  	},err => {
  		alert(err);
  	});*/
  }
  gotoOdds(id,name){
    this.storage.set('odds_id',id);
    this.storage.set('match_name',name);
    this.navCtrl.push(OddsPage,{ id:id });
  }

}

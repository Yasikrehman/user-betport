import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular'; 
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { MorePage } from '../more/more';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
  tab4Root = MorePage;

  constructor(public navCtrl: NavController, public app: App) {

  }

  logout(){
  	const root = this.app.getRootNav();
  	root.popToRoot();
  }
}
